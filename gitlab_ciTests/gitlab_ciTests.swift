//
//  gitlab_ciTests.swift
//  gitlab_ciTests
//
//  Created by Michael Aidoo on 2019-02-24.
//  Copyright © 2019 Michael Aidoo. All rights reserved.
//

import XCTest
@testable import gitlab_ci

class gitlab_ciTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testExample() {
        let a = 1
        let b = 2
        XCTAssertEqual(a + b, 3)
    }
    
    func testSamp() {
        XCTAssertEqual("hi", "hi")
    }
}
