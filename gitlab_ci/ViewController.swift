//
//  ViewController.swift
//  gitlab_ci
//
//  Created by Michael Aidoo on 2019-02-24.
//  Copyright © 2019 Michael Aidoo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func sayHello() -> String {
        return "Hello"
    }

}

